package equation;

import java.math.BigDecimal;
import java.util.function.DoubleUnaryOperator;

public class Equation {
    private final String image;
    private final DoubleUnaryOperator f;
    private final DoubleUnaryOperator df;
    private final DoubleUnaryOperator ddf;

    public Equation(String image, DoubleUnaryOperator f, DoubleUnaryOperator df, DoubleUnaryOperator ddf) {
        this.image = image;
        this.f = f;
        this.df = df;
        this.ddf = ddf;
    }

    public String getImage() {
        return image;
    }
    public double f(double x) {
        return f.applyAsDouble(x);
    }
    public double df(double x) {
        return df.applyAsDouble(x);
    }
    public double ddf(double x) {
        return ddf.applyAsDouble(x);
    }
}
