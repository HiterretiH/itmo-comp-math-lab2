package equation;

import java.util.function.BinaryOperator;
import java.util.function.DoubleUnaryOperator;

public class EquationsSystem {
    private final String image;
    private final BinaryOperator<Double>[] functions;
    private final BinaryOperator<Double>[][] jacobian;
    private final DoubleUnaryOperator[] unaryFunctions;
    private final boolean[] functionFromX;

    public EquationsSystem(String image, BinaryOperator<Double>[] functions, BinaryOperator<Double>[][] jacobian, DoubleUnaryOperator[] unaryFunctions, boolean[] functionFromX) {
        this.image = image;
        this.functions = functions;
        this.jacobian = jacobian;
        this.unaryFunctions = unaryFunctions;
        this.functionFromX = functionFromX;

        if (functions.length != 2 || jacobian.length != 2) {
            throw new IllegalArgumentException();
        }
    }

    public String getImage() {
        return image;
    }

    public double[] negativeFunctions(double x, double y) {
        double[] result = new double[2];

        for (int i = 0; i < 2; i++) {
            result[i] = -functions[i].apply(x, y);
        }

        return result;
    }

    public double[][] jacobian(double x, double y) {
        double[][] result = new double[2][2];

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                result[i][j] = jacobian[i][j].apply(x, y);
            }
        }

        return result;
    }

    public DoubleUnaryOperator[] getUnaryFunctions() {
        return unaryFunctions;
    }

    public boolean[] getFunctionFromX() {
        return functionFromX;
    }
}
