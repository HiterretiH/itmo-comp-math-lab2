package solvers;

import equation.Equation;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class IterationWindow extends SolverWindow {
    private final int COLUMNS = 6;
    private final String[] columnNames = {"№",
            "<html>x<sub>i</sub>",
            "<html>x<sub>i+1</sub>",
            "<html>&phi(x<sub>i+1</sub>)",
            "<html>f(x<sub>i+1</sub>)",
            "<html>|x<sub>i+1</sub> - x<sub>i</sub>|"};
    private String[][] iterations = new String[0][0];
    private double result;
    private double q;
    private double lambda;

    public IterationWindow(Equation equation) {
        super(equation);
    }

    @Override
    protected void solve() {
        Vector<String[]> data = new Vector<>();

        double[] x = new double[2];

        double dfa = equation.df(a);
        double dfb = equation.df(b);

        if (Math.abs(dfa) > Math.abs(dfb)) {
            x[0] = (a + b) / 2;
            lambda = - 1 / dfa;
        }
        else {
            x[0] = (a + b) / 2;
            lambda = - 1 / dfb;
        }
        q = Math.abs(1 + lambda * equation.df(x[0]));

        int i = 0;
        do {
            i++;
            x[i % 2] = x[(i - 1) % 2] + lambda * equation.f(x[(i - 1) % 2]);

            data.add(new String[]{
                    Integer.toString(i),
                    Double.toString(x[(i - 1) % 2]).replace("E", "*10^"),
                    Double.toString(x[i % 2]).replace("E", "*10^"),
                    Double.toString(x[i % 2] + lambda * equation.f(x[i % 2])).replace("E", "*10^"),
                    Double.toString(equation.f(x[i % 2])).replace("E", "*10^"),
                    Double.toString(Math.abs(x[i % 2] - x[(i - 1) % 2])).replace("E", "*10^")
            });
        } while (Math.abs(x[i % 2] - x[(i - 1) % 2]) > epsilon);

        result = x[i % 2];
        iterations = new String[data.size()][COLUMNS];
        for (i = 0; i < data.size(); i++) {
            System.arraycopy(data.get(i), 0, iterations[i], 0, COLUMNS);
        }
    }

    @Override
    protected void showWindow() {
        JFrame frame = new JFrame("Метод простой итерации");
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.getContentPane().setBackground(Color.WHITE);

        JLabel resultLabel = new JLabel();
        JLabel convergenceLabel = new JLabel();
        resultLabel.setFont(new Font("Arial", Font.PLAIN, 24));
        convergenceLabel.setFont(new Font("Arial", Font.PLAIN, 24));

        if (q < 1) {
            convergenceLabel.setText("q = " + q + " < 1. Функция сходится.");
        }
        else {
            convergenceLabel.setText("q = " + q + " > 1. Функция расходится.");
        }

        if (iterations.length > 0) {
            String[] row = iterations[iterations.length - 1];
            if (result >= a && result <= b) {
                resultLabel.setText("<html>" +
                        "x = " + row[2] + "<br>" +
                        "f(x) = " + row[4] + "<br>" +
                        "Количество итераций: " + row[0]
                );
            }
            else {
                resultLabel.setText("<html>" +
                        "Найдено решение за пределами указанного интервала.<br>" +
                        "x = " + row[3] + "<br>" +
                        "f(x) = " + row[4] + "<br>" +
                        "Количество итераций: " + row[0]
                );
            }
        }
        else {
            resultLabel.setText("Решение на заданном интервале не найдено");
        }

        frame.add(new JLabel(new ImageIcon(equation.getImage())));
        frame.add(Box.createRigidArea(new Dimension(0, 10)));
        frame.add(convergenceLabel);
        frame.add(resultLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 10)));
        frame.add(new JScrollPane(new JTable(iterations, columnNames)));

        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
}
