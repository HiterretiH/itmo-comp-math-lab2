package solvers;

import equation.Equation;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class SecantWindow extends SolverWindow {
    private final int COLUMNS = 6;
    private final String[] columnNames = {"№",
            "<html>x<sub>i-1</sub>",
            "<html>x<sub>i</sub>",
            "<html>x<sub>i+1</sub>",
            "<html>f(x<sub>i+1</sub>)",
            "<html>|x<sub>i+1</sub> - x<sub>i</sub>|"};
    private String[][] iterations = new String[0][0];
    private double result;

    public SecantWindow(Equation equation) {
        super(equation);
    }

    @Override
    protected void solve() {
        Vector<String[]> data = new Vector<>();

        double[] x = new double[3];
        if (equation.f(a) * equation.ddf(a) > 0) {
            x[0] = a;
            x[2] = (a + b) / 2;
        }
        else {
            x[0] = b;
            x[2] = (a + b) / 2;
        }

        int i = 0;
        do {
            i++;
            x[i % 3] = x[(i - 1) % 3] - equation.f(x[(i - 1) % 3])
                    * (x[(i - 1) % 3] - x[Math.floorMod(i - 2, 3)])
                    / (equation.f(x[(i - 1) % 3]) - equation.f(x[Math.floorMod(i - 2, 3)]));

            data.add(new String[] {
                    Integer.toString(i),
                    Double.toString(x[Math.floorMod(i - 2, 3)]).replace("E", "*10^"),
                    Double.toString(x[(i - 1) % 3]).replace("E", "*10^"),
                    Double.toString(x[i % 3]).replace("E", "*10^"),
                    Double.toString(equation.f(x[i % 3])).replace("E", "*10^"),
                    Double.toString(Math.abs(x[i % 3] - x[(i - 1) % 3])).replace("E", "*10^")
            });

        } while (Math.abs(x[i % 3] - x[(i - 1) % 3]) > epsilon && Math.abs(equation.f(x[i % 3])) > epsilon);

        result = x[i % 3];
        iterations = new String[data.size()][COLUMNS];
        for (i = 0; i < data.size(); i++) {
            System.arraycopy(data.get(i), 0, iterations[i], 0, COLUMNS);
        }
    }

    @Override
    protected void showWindow() {
        JFrame frame = new JFrame("Метод секущих");
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.getContentPane().setBackground(Color.WHITE);

        JLabel resultLabel = new JLabel();
        resultLabel.setFont(new Font("Arial", Font.PLAIN, 24));

        if (iterations.length > 0) {
            String[] row = iterations[iterations.length - 1];
            if (result >= a && result <= b) {
                resultLabel.setText("<html>" +
                        "x = " + row[3] + "<br>" +
                        "f(x) = " + row[4] + "<br>" +
                        "Количество итераций: " + row[0]
                );
            }
            else {
                resultLabel.setText("<html>" +
                        "Найдено решение за пределами указанного интервала.<br>" +
                        "x = " + row[3] + "<br>" +
                        "f(x) = " + row[4] + "<br>" +
                        "Количество итераций: " + row[0]
                );
            }
        }
        else {
            resultLabel.setText("Решение на заданном интервале не найдено");
        }

        frame.add(new JLabel(new ImageIcon(equation.getImage())));
        frame.add(Box.createRigidArea(new Dimension(0, 10)));
        frame.add(resultLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 10)));
        frame.add(new JScrollPane(new JTable(iterations, columnNames)));

        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
}