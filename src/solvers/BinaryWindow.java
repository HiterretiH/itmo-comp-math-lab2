package solvers;

import equation.Equation;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class BinaryWindow extends SolverWindow {
    private final int COLUMNS = 8;
    private final String[] columnNames = {"№", "a", "b", "x", "f(a)", "f(b)", "f(x)", "b - a"};
    private String[][] iterations = new String[0][0];

    public BinaryWindow(Equation equation) {
        super(equation);
    }

    @Override
    protected void solve() {
        if (equation.f(a) * equation.f(b) > 0) return;

        Vector<String[]> data = new Vector<>();

        double a = this.a;
        double b = this.b;
        int iteration = 0;
        double x;

        do {
            x = (a + b) / 2;
            iteration++;

            data.add(new String[] {
                    Integer.toString(iteration),
                    Double.toString(a).replace("E", "*10^"),
                    Double.toString(b).replace("E", "*10^"),
                    Double.toString(x).replace("E", "*10^"),
                    Double.toString(equation.f(a)).replace("E", "*10^"),
                    Double.toString(equation.f(b)).replace("E", "*10^"),
                    Double.toString(equation.f(x)).replace("E", "*10^"),
                    Double.toString(b - a).replace("E", "*10^")
            });

            if (equation.f(x) * equation.f(a) < 0) {
                b = x;
            }
            else {
                a = x;
            }
        } while (b - a > epsilon && Math.abs(equation.f(x)) > epsilon);

        iterations = new String[data.size()][COLUMNS];
        for (int i = 0; i < data.size(); i++) {
            System.arraycopy(data.get(i), 0, iterations[i], 0, COLUMNS);
        }
    }

    @Override
    protected void showWindow() {
        JFrame frame = new JFrame("Метод половинного деления");
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.getContentPane().setBackground(Color.WHITE);

        JLabel resultLabel = new JLabel();
        resultLabel.setFont(new Font("Arial", Font.PLAIN, 24));

        if (iterations.length > 0) {
            String[] row = iterations[iterations.length - 1];
            resultLabel.setText("<html>" +
                    "x = " + row[3] + "<br>" +
                    "f(x) = " + row[6] + "<br>" +
                    "Количество итераций: " + row[0]
            );
        }
        else {
            resultLabel.setText("Единственное решение на заданном интервале не найдено");
        }

        frame.add(new JLabel(new ImageIcon(equation.getImage())));
        frame.add(Box.createRigidArea(new Dimension(0, 10)));
        frame.add(resultLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 10)));
        frame.add(new JScrollPane(new JTable(iterations, columnNames)));

        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
}
