package solvers;

import equation.EquationsSystem;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class NewtonWindow extends SystemSolverWindow {
    private final int COLUMNS = 5;
    private final String[] columnNames = {"№", "x", "y", "<html>&#916;x", "<html>&#916;y"};
    private String[][] iterations = new String[0][0];

    public NewtonWindow(EquationsSystem system) {
        super(system);
    }

    @Override
    protected void solve() {
        Vector<String[]> data = new Vector<>();

        double deltaX;
        double deltaY;

        int i = 0;

        do {
            i++;

            double[][] a = system.jacobian(x, y);
            double[] b = system.negativeFunctions(x, y);

            deltaX = (a[1][1] * b[0] - a[0][1] * b[1]) / (a[1][1] * a[0][0] - a[0][1] * a[1][0]);
            deltaY = (a[0][0] * b[1] - b[0] * a[1][0]) / (a[1][1] * a[0][0] - a[0][1] * a[1][0]);

            x += deltaX;
            y += deltaY;

            data.add(new String[] {
                    Integer.toString(i),
                    Double.toString(x).replace("E", "*10^"),
                    Double.toString(y).replace("E", "*10^"),
                    Double.toString(deltaX).replace("E", "*10^"),
                    Double.toString(deltaY).replace("E", "*10^")
            });

        } while (Math.abs(deltaX) > epsilon || Math.abs(deltaY) > epsilon);

        iterations = new String[data.size()][COLUMNS];
        for (i = 0; i < data.size(); i++) {
            System.arraycopy(data.get(i), 0, iterations[i], 0, COLUMNS);
        }
    }

    @Override
    protected void showWindow() {
        JFrame frame = new JFrame("Метод Ньютона");
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.getContentPane().setBackground(Color.WHITE);

        JLabel resultLabel = new JLabel();
        resultLabel.setFont(new Font("Arial", Font.PLAIN, 24));

        if (iterations.length > 0) {
            String[] row = iterations[iterations.length - 1];
            resultLabel.setText("<html>" +
                    "x = " + row[1] + "<br>" +
                    "y = " + row[2] + "<br>" +
                    "Количество итераций: " + row[0]
            );
        }
        else {
            resultLabel.setText("Решение на заданном интервале не найдено");
        }

        frame.add(new JLabel(new ImageIcon(system.getImage())));
        frame.add(Box.createRigidArea(new Dimension(0, 10)));
        frame.add(resultLabel);
        frame.add(Box.createRigidArea(new Dimension(0, 10)));
        frame.add(new JScrollPane(new JTable(iterations, columnNames)));

        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
}
