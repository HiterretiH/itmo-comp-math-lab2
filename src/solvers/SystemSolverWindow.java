package solvers;

import equation.Equation;
import equation.EquationsSystem;

public abstract class SystemSolverWindow {
    protected final EquationsSystem system;
    protected double x = 0;
    protected double y = 0;
    protected double epsilon = 1;

    public SystemSolverWindow(EquationsSystem system) {
        this.system = system;
    }

    public void setParameters(double x, double y, double epsilon) {
        this.x = x;
        this.y = y;
        this.epsilon = epsilon;
    }

    public void run() {
        solve();
        showWindow();
    }

    protected abstract void solve();
    protected abstract void showWindow();
}
