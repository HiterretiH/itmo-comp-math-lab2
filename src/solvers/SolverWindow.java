package solvers;

import equation.Equation;

public abstract class SolverWindow {
    protected final Equation equation;
    protected double a = 0;
    protected double b = 0;
    protected double epsilon = 1;

    public SolverWindow(Equation equation) {
        this.equation = equation;
    }

    public void setParameters(double a, double b, double epsilon) {
        this.a = a;
        this.b = b;
        this.epsilon = epsilon;
    }

    public void run() {
        solve();
        showWindow();
    }

    protected abstract void solve();
    protected abstract void showWindow();
}
