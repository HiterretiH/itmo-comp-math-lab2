import equation.EquationsSystem;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.ImageTitle;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import solvers.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.function.DoubleUnaryOperator;

public class SystemWindow {
    private final JFrame frame;
    private final EquationsSystem system;
    private final JTextField xField;
    private final JTextField yField;
    private final JTextField epsilonField;

    public SystemWindow(EquationsSystem system) {
        this.system = system;
        frame = new JFrame("Система уравнений");

        GroupLayout layout = new GroupLayout(frame.getContentPane());
        frame.setLayout(layout);

        ChartPanel chart = generateChart();

        JLabel intervalLabel = new JLabel("Начальное приближение:");
        JLabel xLabel = new JLabel("<html>x<sub>0</sub>: ");
        JLabel yLabel = new JLabel("<html>y<sub>0</sub>: ");
        JLabel epsilonLabel = new JLabel("<html>Точность &epsilon;: ");

        JLabel methodsLabel = new JLabel("Выбор метода:");

        xField = new JTextField("0");
        yField = new JTextField("0");
        epsilonField = new JTextField("0,01");

        JButton button1 = new JButton("Метод Ньютона");

        button1.addActionListener(e -> solveSystem(new NewtonWindow(system)));

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addComponent(chart)
                        .addGroup(layout.createParallelGroup()
                                .addComponent(intervalLabel)
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(xLabel)
                                        .addComponent(xField)
                                )
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(yLabel)
                                        .addComponent(yField)
                                )
                                .addComponent(epsilonLabel)
                                .addComponent(epsilonField)
                                .addComponent(methodsLabel)
                                .addComponent(button1)
                        )
        );

        layout.setVerticalGroup(
                layout.createParallelGroup()
                        .addComponent(chart)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(intervalLabel)
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(xLabel)
                                        .addComponent(xField, 15, 20, 25)
                                )
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(yLabel)
                                        .addComponent(yField, 15, 20, 25)
                                )
                                .addComponent(epsilonLabel)
                                .addComponent(epsilonField, 15, 20, 25)
                                .addComponent(methodsLabel)
                                .addComponent(button1)
                        )
        );

        frame.getContentPane().setBackground(Color.WHITE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    private void solveSystem(SystemSolverWindow solverWindow) {
        try {
            double x = validateDouble(xField.getText(), "<html>x<sub>0</sub> должен быть числом");
            double y = validateDouble(yField.getText(), "<html>y<sub>0</sub> должен быть числом");
            double epsilon = validateDouble(epsilonField.getText(), "<html>Точность &epsilon; должна быть числом от 0 до 1", 0, 1);

            solverWindow.setParameters(x, y, epsilon);
            solverWindow.run();
        }
        catch (NumberFormatException e) {
            return;
        }
    }

    private double validateDouble(String text, String errorMessage) {
        try {
            return Double.parseDouble(text.replace(',', '.'));
        }
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, errorMessage, "", JOptionPane.ERROR_MESSAGE);
            throw new NumberFormatException(errorMessage);
        }
    }

    private double validateDouble(String text, String errorMessage, double from, double to) {
        double result = validateDouble(text, errorMessage);

        if (result > from && result < to) {
            return result;
        }
        else {
            JOptionPane.showMessageDialog(frame, errorMessage, "", JOptionPane.ERROR_MESSAGE);
            throw new NumberFormatException(errorMessage);
        }
    }

    private ChartPanel generateChart() {
        XYDataset dataset = createDataset();

        JFreeChart chart = ChartFactory.createXYLineChart("", "x", "y", dataset);

        ImageIcon image = new ImageIcon(system.getImage());
        chart.addSubtitle(new ImageTitle(image.getImage()));

        customizeChart(chart);

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(800, 600));
        chartPanel.setMouseZoomable(true);
        chartPanel.setMouseWheelEnabled(true);

        return chartPanel;
    }

    private XYDataset createDataset() {
        XYSeriesCollection dataset = new XYSeriesCollection();

        DoubleUnaryOperator[] functions = system.getUnaryFunctions();
        boolean[] functionFromX = system.getFunctionFromX();
        XYSeries[] series = new XYSeries[functions.length];

        for (int i = 0; i < functions.length; i++) {
            series[i] = new XYSeries("Функция " + (i + 1), false);

            double[][] data = generateData(-10, 10, functions[i]);

            for (int j = 0; j < data[0].length; j++) {
                if (functionFromX[i]) {
                    series[i].add(data[0][j], data[1][j]);
                }
                else {
                    series[i].add(data[1][j], data[0][j]);
                }
            }

            dataset.addSeries(series[i]);
        }

        return dataset;
    }

        private void customizeChart(JFreeChart chart) {
            XYPlot plot = chart.getXYPlot();

            plot.setRangeZeroBaselineVisible(true);
            plot.setDomainZeroBaselineVisible(true);

            NumberAxis xAxis = new NumberAxis("x");
            NumberAxis yAxis = new NumberAxis("y");

            xAxis.setRange(-10, 10);
            yAxis.setRange(-10, 10);

            plot.setDomainAxis(xAxis);
            plot.setRangeAxis(yAxis);
        }

    private double[][] generateData(double a, double b, DoubleUnaryOperator function) {
        java.util.List<Double> x = new ArrayList<>();
        java.util.List<Double> y = new ArrayList<>();

        for (int i = 0; a < b; i++) {
            x.add(a);
            y.add(function.applyAsDouble(a));

            a += 0.005;
        }
        x.add(b);
        y.add(function.applyAsDouble(b));

        double[][] data = new double[2][x.size()];
        for (int i = 0; i < x.size(); i++) {
            data[0][i] = x.get(i);
            data[1][i] = y.get(i);
        }

        return data;
    }
}
