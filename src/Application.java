import equation.Equation;
import equation.EquationsSystem;
import solvers.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class Application {
    private final JFrame frame;

    public Application(List<Equation> equations, List<EquationsSystem> systems) {
        /*
        Add images. Bind image click => open a new EquationWindow
         */

        frame = new JFrame("Lab 2");

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.getContentPane().setBackground(Color.WHITE);

        JLabel equationsLabel = new JLabel("Выберите уравнение для решения:");
        equationsLabel.setFont(new Font("Arial", Font.PLAIN, 24));
        frame.add(equationsLabel);
        frame.add(Box.createVerticalStrut(5));

        for (Equation eq : equations) {
            ImageIcon icon = new ImageIcon(eq.getImage());
            JButton button = new JButton(icon);
            button.setMargin(new Insets(0, 0, 0, 0));
            button.setBackground(null);

            button.addActionListener(
                    e -> {
                        new EquationWindow(eq);
                    }
            );

            frame.add(button);
            frame.add(Box.createVerticalStrut(5));
        }

        frame.add(Box.createVerticalStrut(20));
        JLabel systemsLabel = new JLabel("Или выберите систему для решения:");
        systemsLabel.setFont(new Font("Arial", Font.PLAIN, 24));
        frame.add(systemsLabel);
        frame.add(Box.createVerticalStrut(5));

        for (EquationsSystem system : systems) {
            ImageIcon icon = new ImageIcon(system.getImage());
            JButton button = new JButton(icon);
            button.setMargin(new Insets(0, 0, 0, 0));
            button.setBackground(null);

            button.addActionListener(
                    e -> {
                        new SystemWindow(system);
                    }
            );

            frame.add(button);
            frame.add(Box.createVerticalStrut(5));
        }

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
}
