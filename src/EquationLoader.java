import equation.Equation;
import equation.EquationsSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.DoubleUnaryOperator;

public class EquationLoader {
    public static List<Equation> getEquations() {
        List<Equation> equations = new ArrayList<>(4);


        equations.add(new Equation(
                "images/1.png",
                x -> x*x*x + 2.28 * x*x - 1.934 * x - 3.907,
                x -> -1.934 + 4.56 * x + 3 * x * x,
                x -> 4.56 + 6 * x
        ));

        equations.add(new Equation(
                "images/2.png",
                x -> Math.sin(x * x),
                x -> 2 * x * Math.cos(x * x),
                x -> 2 * (Math.cos(x * x) - 2 * x * x * Math.sin(x * x))
        ));

        equations.add(new Equation(
                "images/3.png",
                x -> Math.exp(x) - x*x*x - 1,
                x -> Math.exp(x) - 3 * x * x,
                x -> Math.exp(x) - 6 * x
        ));

        return equations;
    }

    public static List<EquationsSystem> getEquationSystems() {
        List<EquationsSystem> systems = new ArrayList<>(4);

        ArrayList<BinaryOperator<Double>> functions1 = new ArrayList<>();
        functions1.add((x, y) -> 2*x - Math.sin(y) - 1.5);
        functions1.add((x, y) -> y + Math.cos(x) - 1.5);

        ArrayList<ArrayList<BinaryOperator<Double>>> jacobian1 = new ArrayList<>();
        jacobian1.add(new ArrayList<>(2));
        jacobian1.add(new ArrayList<>(2));
        jacobian1.get(0).add((x, y) -> 2d);
        jacobian1.get(0).add((x, y) -> -Math.cos(y));
        jacobian1.get(1).add((x, y) -> -Math.sin(x));
        jacobian1.get(1).add((x, y) -> 1d);

        ArrayList<BinaryOperator<Double>> functions2 = new ArrayList<>();
        functions2.add((x, y) -> x * x - y - 9);
        functions2.add((x, y) -> Math.exp(x) - 10 * y - 3);

        ArrayList<ArrayList<BinaryOperator<Double>>> jacobian2 = new ArrayList<>();
        jacobian2.add(new ArrayList<>(2));
        jacobian2.add(new ArrayList<>(2));
        jacobian2.get(0).add((x, y) -> 2 * x);
        jacobian2.get(0).add((x, y) -> -1d);
        jacobian2.get(1).add((x, y) -> Math.exp(x));
        jacobian2.get(1).add((x, y) -> -10d);

        systems.add(new EquationsSystem(
                "images/s1.png",
                functions1.toArray(new BinaryOperator[functions1.size()]),
                jacobian1.stream()
                        .map(list -> list.toArray(new BinaryOperator[0]))
                        .toArray(BinaryOperator[][]::new),
                new DoubleUnaryOperator[]{
                        x -> 1.5 - Math.cos(x),
                        y -> 0.5 * Math.sin(y) + 0.75
                },
                new boolean[] {
                        true,
                        false
                }
        ));

        systems.add(new EquationsSystem(
                "images/s2.png",
                functions2.toArray(new BinaryOperator[functions1.size()]),
                jacobian2.stream()
                        .map(list -> list.toArray(new BinaryOperator[0]))
                        .toArray(BinaryOperator[][]::new),
                new DoubleUnaryOperator[]{
                        x -> x * x - 9,
                        x -> 0.1 * Math.exp(x) - 0.3
                },
                new boolean[] {
                        true,
                        true
                }
        ));

        return systems;
    }
}
