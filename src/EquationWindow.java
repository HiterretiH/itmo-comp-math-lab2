import equation.Equation;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.title.ImageTitle;
import org.jfree.data.xy.DefaultXYDataset;
import solvers.*;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class EquationWindow {
    private final JFrame frame;
    private final Equation equation;
    private final JTextField fromField;
    private final JTextField toField;
    private final JTextField epsilonField;

    public EquationWindow(Equation equation) {
        this.equation = equation;
        frame = new JFrame("Уравнение");

        GroupLayout layout = new GroupLayout(frame.getContentPane());
        frame.setLayout(layout);

        ChartPanel chart = generateChart();

        JLabel intervalLabel = new JLabel("Интервал поиска:");
        JLabel fromLabel = new JLabel("От: ");
        JLabel toLabel = new JLabel("До: ");
        JLabel epsilonLabel = new JLabel("<html>Точность &epsilon;: ");

        JLabel methodsLabel = new JLabel("Выбор метода:");

        fromField = new JTextField("0");
        toField = new JTextField("1");
        epsilonField = new JTextField("0,01");

        JButton button1 = new JButton("Метод половинного деления");
        JButton button2 = new JButton("Метод секущих");
        JButton button3 = new JButton("Метод простой итерации");

        button1.addActionListener(e -> solveEquation(new BinaryWindow(equation)));
        button2.addActionListener(e -> solveEquation(new SecantWindow(equation)));
        button3.addActionListener(e -> solveEquation(new IterationWindow(equation)));

        layout.setHorizontalGroup(
                layout.createSequentialGroup()
                        .addComponent(chart)
                        .addGroup(layout.createParallelGroup()
                                .addComponent(intervalLabel)
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(fromLabel)
                                        .addComponent(fromField)
                                )
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(toLabel)
                                        .addComponent(toField)
                                )
                                .addComponent(epsilonLabel)
                                .addComponent(epsilonField)
                                .addComponent(methodsLabel)
                                .addComponent(button1)
                                .addComponent(button2)
                                .addComponent(button3)
                        )
        );

        layout.setVerticalGroup(
                layout.createParallelGroup()
                        .addComponent(chart)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(intervalLabel)
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(fromLabel)
                                        .addComponent(fromField, 15, 20, 25)
                                )
                                .addGroup(layout.createParallelGroup()
                                        .addComponent(toLabel)
                                        .addComponent(toField, 15, 20, 25)
                                )
                                .addComponent(epsilonLabel)
                                .addComponent(epsilonField, 15, 20, 25)
                                .addComponent(methodsLabel)
                                .addComponent(button1)
                                .addComponent(button2)
                                .addComponent(button3)
                        )
        );

        frame.getContentPane().setBackground(Color.WHITE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }

    private void solveEquation(SolverWindow solverWindow) {
        try {
            double a = validateDouble(fromField.getText(), "Начало интервала должно быть числом");
            double b = validateDouble(toField.getText(), "Конец интервала должен быть числом");
            double epsilon = validateDouble(epsilonField.getText(), "<html>Точность &epsilon; должна быть числом от 0 до 1", 0, 1);

            solverWindow.setParameters(a, b, epsilon);
            solverWindow.run();
        }
        catch (NumberFormatException e) {
            return;
        }
    }

    private double validateDouble(String text, String errorMessage) {
        try {
            return Double.parseDouble(text.replace(',', '.'));
        }
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(frame, errorMessage, "", JOptionPane.ERROR_MESSAGE);
            throw new NumberFormatException(errorMessage);
        }
    }

    private double validateDouble(String text, String errorMessage, double from, double to) {
        double result = validateDouble(text, errorMessage);

        if (result > from && result < to) {
            return result;
        }
        else {
            JOptionPane.showMessageDialog(frame, errorMessage, "", JOptionPane.ERROR_MESSAGE);
            throw new NumberFormatException(errorMessage);
        }
    }

    private ChartPanel generateChart() {
        DefaultXYDataset dataset = new DefaultXYDataset();
        dataset.addSeries("Функция", generateData(-10, 10));

        JFreeChart chart = ChartFactory.createXYLineChart(
                "",
                "x",
                "f(x)",
                dataset
        );

        ImageIcon image = new ImageIcon(equation.getImage());
        chart.addSubtitle(new ImageTitle(image.getImage()));

        chart.getXYPlot().setRangeZeroBaselineVisible(true);
        chart.getXYPlot().setDomainZeroBaselineVisible(true);

        NumberAxis xAxis = new NumberAxis("x");
        NumberAxis yAxis = new NumberAxis("f(x)");

        xAxis.setRange(-10, 10);
        yAxis.setRange(-10, 10);

        chart.getXYPlot().setDomainAxis(xAxis);
        chart.getXYPlot().setRangeAxis(yAxis);

        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(800, 600));
        chartPanel.setMouseZoomable(false);

        return chartPanel;
    }

    private double[][] generateData(double a, double b) {
        java.util.List<Double> x = new ArrayList<>();
        java.util.List<Double> y = new ArrayList<>();

        for (int i = 0; a < b; i++) {
            x.add(a);
            y.add(equation.f(a));

            double f1 = Math.abs(equation.f(a));
            double f2 = Math.abs(equation.f(a + 0.01));
            if (f1 < 1 && f1 > 0.0001) {
                a += 0.01 * f1;
            }
            else if (f2 < 1) {
                a += Math.max(0.000001, 0.0001 * f2);
            }
            else {
                a += 0.01;
            }
        }
        x.add(b);
        y.add(equation.f(b));

        double[][] data = new double[2][x.size()];
        for (int i = 0; i < x.size(); i++) {
            data[0][i] = x.get(i);
            data[1][i] = y.get(i);
        }
        return data;
    }
}
