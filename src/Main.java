import equation.Equation;

import java.math.BigDecimal;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {


        new Application(EquationLoader.getEquations(), EquationLoader.getEquationSystems());
    }
}